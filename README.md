# Customer Feedback System

Despite claims and ad campaigns to the contrary, many companies are doing a poor job of establishing productive dialogues with their customers. A simple but effective way to help remedy this is to run regular customer focus groups. A focus group is a